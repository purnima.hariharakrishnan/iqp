@testAutomate
Feature: testAutomate
Scenario: Test recorder actions - testAutomate.feature
Given I navigate to "(url)"
And I click on "[right click me]" link
And I click on "[right click me]" link
And I click on "[right click me]" link
And I click on "[Edit]" link
And I click on "[Selenium]" link
And I click on "[Ajax Demo]" link
And I click on "[SEO]" link
And I click on "[Page-1]" link
And I click on "[Selenium]" link
And I click on "[File Upload]" link
And I upload to "[uploadfile_0]" the file "(column_1)"
And I click on "[terms]" link
And I click on "[submitbutton]" link
And I click on "[Selenium]" link
And I click on "[Login]" link
And I enter into input field "[email]" the value "(column_2)"
And I enter into input field "[email]" the value "(column_3)"
And I enter into input field "[passwd]" the value "(column_4)"
And I click on "[Sign in]" link
