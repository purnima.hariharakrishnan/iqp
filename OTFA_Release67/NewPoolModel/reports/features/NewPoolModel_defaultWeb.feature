@OTFA_Release67 @1 @OTFA_Release67_Testing @subFunc
Feature: FeatureFile
Scenario: 1
Given I navigate to "urlTest"
When I validate that the current url ends with "urlTest"
Then I validate that the current url ends with "urlTest"
And I group steps into function as "HL2"
And I search for OTP in mail "otp2" and store in otp variable "otpValue2"
And I end function
And I group steps into function as "HL1"
Given I enter into otp field "otp" the otp variable "otpValue"
And I end function
When I validate that the current url ends with "urlTest"
Then I validate that the current url ends with "urlTest"
And I group steps into function as "HL2"
And I search for OTP in mail "otp2" and store in otp variable "otpValue2"
And I end function

Scenario: 2
Given I navigate to "urlTest"
When I validate that the current url ends with "urlTest"
Then I validate that the current url ends with "urlTest"
And I group steps into function as "HL2"
And I search for OTP in mail "otp2" and store in otp variable "otpValue2"
And I end function
And I group steps into function as "HL1"
Given I enter into otp field "otp" the otp variable "otpValue"
And I end function
When I validate that the current url ends with "urlTest"
Then I validate that the current url ends with "urlTest"
And I group steps into function as "HL2"
And I search for OTP in mail "otp2" and store in otp variable "otpValue2"
And I end function

