@OTFA_Release67 @1 @OTFA_Release67_Testing @OldProd
Feature: Simple flow sub
Scenario: change scenario
Given I navigate to "http://simplephil-webtesting.blogspot.in/"
And I group steps into function as "HLStepWithComments"
And I add wait seconds of "1"
And I wait for "5" seconds
And I continue execution on failure of following step
When I run custom code "ParameterizationCustomCode" with arguments
|"[firstname]"|
|"(firstname)"|
|"$Menu$"|
|"%8FO5Ptg%2FOc%2BCfaTi5zkTrA%3D%3D%"|
And I capture screenshot
And I add wait seconds of "5"
And I capture screenshot
And I wait for "2" seconds
And I take screenshot
And I capture screenshot
And I end function
And I enter into input field "[Search]" the value "Good Day"
And I add wait seconds of "2"
And I take screenshot
And I group steps into function as "WebCustFunc"
And I wait for "5" seconds
And I continue execution on failure of following step
And I navigate to "https://www.google.com"
And I capture screenshot
And I wait for "10" seconds
And I enter into input field "[gglSearch]" the value "(SrchVal)" and search
And I capture screenshot
And I click on "new" button
And I capture screenshot
And I wait for "3" seconds
And I continue execution on failure of following step
And I take screenshot
And I capture screenshot
And I end function

