﻿@OTFA_TG_Regression @5.10 @OTFA_TG_Regression_Test @task
Feature: Validate Users Menu1
Scenario: Scenario to validate users menu from IQP main menu1
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 1
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu2
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 2
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu3
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 3
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu4
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 4
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu5
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 5
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu6
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 6
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu7
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 7
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu8
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 8
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu9
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 9
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu10
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 10
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu11
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 11
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu12
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 12
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu13
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 13
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu14
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 14
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu15
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 15
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu16
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 16
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu17
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 17
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu18
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 18
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu19
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 19
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu20
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 20
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu21
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 21
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu22
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 22
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu23
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 23
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu24
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 24
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu25
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 25
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu26
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 26
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu27
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 27
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu28
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 28
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu29
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 29
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu30
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 30
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu31
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 31
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu32
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 32
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu33
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 33
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu34
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 34
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu35
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 35
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu36
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 36
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu37
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 37
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu38
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 38
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu39
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 39
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu40
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 40
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu41
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 41
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu42
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 42
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu43
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 43
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu44
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 44
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu45
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 45
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu46
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 46
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu47
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 47
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu48
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 48
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu49
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 49
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu50
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 50
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu51
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 51
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu52
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 52
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu53
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 53
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu54
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 54
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu55
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 55
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

Scenario: Scenario to validate users menu from IQP main menu56
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"
Scenario: 56
Given I navigate to "[https://platform-app-ignite-qp.mig-cluster-01-7e2996fc95fd6eb4a4c7a63aa3e73699-0000.us-south.containers.appdomain.cloud/ignitePlatform/]"

