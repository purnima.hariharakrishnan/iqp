@Regression @DataParameterization
Feature: to fetch the policy details v6
@DataParameterizationWeb11
Scenario: Validate data parameterization - 1
Given I navigate to "(WebsiteURL)"
And I enter into input field "[PolicyNumberTextBox]" the value "%(PolicyNumber)%"
And I enter into input field "[DoBTextBox]" the value "(DoB)"
And I take screenshot
And I click on "[Submit]" button
