﻿@Sample1 @1 @Sample1_Test @Trad
Feature: MilePost_NonTraditional_Flow
Scenario: 1_Validate for Product Category TN ,Business Event PLN,WMA TRansaction code Blank,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 2_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code TAI,Memo code G2 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 3_Validate for Product Category TN ,Business Event CLS,WMA TRansaction code PA,Memo code G2 and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 4_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code PA,Memo code IC and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 5_Validate for Product Category TN ,Business Event PRP,WMA TRansaction code NA,Memo code R5 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 6_Validate for Product Category TN ,Business Event SFU,WMA TRansaction code TA,Memo code D1 and Amount Type AH
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 7_Validate for Product Category TP ,Business Event MAT,WMA TRansaction code FNM,Memo code D1 and Amount Type H
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 8_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code NFX,Memo code G1 and Amount Type A1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 9_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code TA,Memo code Blank and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 10_Validate for Product Category TP ,Business Event SPA,WMA TRansaction code KP,Memo code AL and Amount Type AF
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 11_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code PJ,Memo code UC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 12_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SR,Memo code G1 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 13_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code NE,Memo code R5 and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 14_Validate for Product Category TP ,Business Event LFL,WMA TRansaction code TL,Memo code Blank and Amount Type D
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 15_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code GH,Memo code Blank and Amount Type GC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 16_Validate for Product Category TP ,Business Event RPP,WMA TRansaction code RA1,Memo code R5 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 17_Validate for Product Category TN ,Business Event DCF,WMA TRansaction code PHF,Memo code G2 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 18_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code KF,Memo code IC and Amount Type J
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 19_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code PHP,Memo code IC and Amount Type YA
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 20_Validate for Product Category TP ,Business Event DBP,WMA TRansaction code NG,Memo code G1 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 21_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NP,Memo code G1 and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 22_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code PHF,Memo code AL and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 23_Validate for Product Category TP ,Business Event CLS,WMA TRansaction code OMB,Memo code IC and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 24_Validate for Product Category TP ,Business Event PLN,WMA TRansaction code TCI,Memo code IC and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 25_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code TC,Memo code D1 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 26_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code RA,Memo code IC and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 27_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code SA,Memo code D1 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 28_Validate for Product Category TN ,Business Event CLS,WMA TRansaction code TW,Memo code G1 and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 29_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code TW,Memo code AL and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 30_Validate for Product Category TP ,Business Event SPA,WMA TRansaction code TWI,Memo code IC and Amount Type AF
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 31_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code NA,Memo code AL and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 32_Validate for Product Category TP ,Business Event SPA,WMA TRansaction code KE,Memo code D1 and Amount Type AF
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 33_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code SR,Memo code IC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 34_Validate for Product Category TP ,Business Event SFU,WMA TRansaction code TAI,Memo code Blank and Amount Type AF
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 35_Validate for Product Category TN ,Business Event CLS,WMA TRansaction code PHF,Memo code D1 and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 36_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code TL,Memo code R5 and Amount Type A1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 37_Validate for Product Category TP ,Business Event PLN,WMA TRansaction code NE,Memo code G1 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 38_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code KI,Memo code Blank and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 39_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NAI,Memo code R5 and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 40_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code PHF,Memo code Blank and Amount Type A1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 41_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NV,Memo code G1 and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 42_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SC,Memo code G1 and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 43_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code RA1,Memo code IC and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 44_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SP,Memo code R5 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 45_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code KP,Memo code Blank and Amount Type FC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 46_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code SR,Memo code Blank and Amount Type I
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 47_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code NE,Memo code IC and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 48_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code FNM,Memo code UC and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 49_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code TAI,Memo code D1 and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 50_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code FNM,Memo code IC and Amount Type A1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 51_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code TL,Memo code D1 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 52_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code FNM,Memo code AL and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 53_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code NFX,Memo code Blank and Amount Type J
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 54_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code NE,Memo code Blank and Amount Type YA
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 55_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NFX,Memo code AL and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 56_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code PA,Memo code R5 and Amount Type YA
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 57_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code TA,Memo code G1 and Amount Type YA
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 58_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code KE,Memo code G1 and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 59_Validate for Product Category TP ,Business Event PRW,WMA TRansaction code ND,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 60_Validate for Product Category TP ,Business Event PCC,WMA TRansaction code TD,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 61_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code PJ,Memo code AL and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 62_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code TA,Memo code AL and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 63_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code PA,Memo code D1 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 64_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code GH,Memo code G2 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 65_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code KF,Memo code R5 and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 66_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code FNM,Memo code R5 and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 67_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code PJ,Memo code IC and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 68_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code KP,Memo code IC and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 69_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code GH,Memo code IC and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 70_Validate for Product Category TP ,Business Event DCF,WMA TRansaction code PB,Memo code G2 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 71_Validate for Product Category TP ,Business Event SPA,WMA TRansaction code GLD,Memo code D1 and Amount Type AF
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 72_Validate for Product Category TN ,Business Event DCF,WMA TRansaction code PHP,Memo code G2 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 73_Validate for Product Category TN ,Business Event PRP,WMA TRansaction code NF,Memo code R5 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 74_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SN,Memo code R5 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 75_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code PA,Memo code Blank and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 76_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code KF,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 77_Validate for Product Category TN ,Business Event CLS,WMA TRansaction code TA,Memo code IC and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 78_Validate for Product Category TP ,Business Event SPA,WMA TRansaction code KF,Memo code D1 and Amount Type AF
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 79_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code TA,Memo code R5 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 80_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code TL,Memo code IC and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 81_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code KE,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 82_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code TWI,Memo code AL and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 83_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code PA,Memo code G1 and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 84_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code TAI,Memo code R5 and Amount Type A1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 85_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code PHF,Memo code IC and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 86_Validate for Product Category TP ,Business Event SPA,WMA TRansaction code TW,Memo code D1 and Amount Type AF
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 87_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code TAI,Memo code IC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 88_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code KP,Memo code G1 and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 89_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NE,Memo code AL and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 90_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code TA,Memo code G2 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 91_Validate for Product Category TP ,Business Event CLS,WMA TRansaction code PHP,Memo code D1 and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 92_Validate for Product Category TP ,Business Event CLS,WMA TRansaction code FNM,Memo code G2 and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 93_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code NFX,Memo code R5 and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 94_Validate for Product Category TP ,Business Event CLS,WMA TRansaction code PJ,Memo code G2 and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 95_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code PJ,Memo code R5 and Amount Type A1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 96_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code NFX,Memo code G2 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 97_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code KP,Memo code D1 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 98_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code TW,Memo code IC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 99_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code TA,Memo code UC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 100_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code TWI,Memo code G2 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 101_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code NFX,Memo code IC and Amount Type I
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 102_Validate for Product Category TN ,Business Event PRP,WMA TRansaction code GH,Memo code G1 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 103_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SA,Memo code IC and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 104_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SP,Memo code G1 and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 105_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SR,Memo code R5 and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 106_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SC,Memo code R5 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 107_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code TL,Memo code G1 and Amount Type YA
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 108_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code SR,Memo code D1 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 109_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code RA,Memo code G1 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 110_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code RA1,Memo code G1 and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 111_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code TAI,Memo code G1 and Amount Type YA
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 112_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code TC,Memo code UC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 113_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code PA,Memo code AL and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 114_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SN,Memo code IC and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 115_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NP,Memo code IC and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 116_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code KP,Memo code UC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 117_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code PHP,Memo code G1 and Amount Type A1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 118_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code TAI,Memo code AL and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 119_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code GH,Memo code R5 and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 120_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code GH,Memo code AL and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 121_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code PJ,Memo code Blank and Amount Type YA
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 122_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code KF,Memo code AL and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 123_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NAI,Memo code G1 and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 124_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code FNM,Memo code G1 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 125_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code PHF,Memo code G1 and Amount Type YA
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 126_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code KP,Memo code R5 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 127_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code NA,Memo code G1 and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 128_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code KF,Memo code G1 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 129_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code KE,Memo code IC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 130_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code FNM,Memo code Blank and Amount Type YA
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 131_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NA,Memo code IC and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 132_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code NFX,Memo code D1 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 133_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code NE,Memo code D1 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 134_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code KE,Memo code AL and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 135_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NP,Memo code AL and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 136_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code TWI,Memo code G1 and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 137_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code PJ,Memo code G1 and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 138_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code PHF,Memo code R5 and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 139_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NP,Memo code R5 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 140_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code KE,Memo code R5 and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 141_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NAI,Memo code AL and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 142_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NAI,Memo code IC and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 143_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code TWI,Memo code R5 and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 144_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NV,Memo code R5 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 145_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NV,Memo code AL and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 146_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NV,Memo code IC and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 147_Validate for Product Category TN ,Business Event SPA,WMA TRansaction code Blank,Memo code Blank and Amount Type AF
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 148_Validate for Product Category TN ,Business Event PLR,WMA TRansaction code Blank,Memo code Blank and Amount Type A1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 149_Validate for Product Category TN ,Business Event LFL,WMA TRansaction code Blank,Memo code Blank and Amount Type D
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 150_Validate for Product Category TP ,Business Event CLS,WMA TRansaction code TAI,Memo code IC and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 151_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code TAI,Memo code Blank and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 152_Validate for Product Category TP ,Business Event LFL,WMA TRansaction code NFX,Memo code Blank and Amount Type D
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 153_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code PA,Memo code Blank and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 154_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code PHF,Memo code Blank and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 155_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code KP,Memo code Blank and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 156_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code TL,Memo code Blank and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 157_Validate for Product Category TP ,Business Event SFU,WMA TRansaction code TAI,Memo code IC and Amount Type AH
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 158_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code PA,Memo code IC and Amount Type FC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 159_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code NE,Memo code IC and Amount Type GC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 160_Validate for Product Category TN ,Business Event PLR,WMA TRansaction code Blank,Memo code Blank and Amount Type YA
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 161_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code TA,Memo code R5 and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 162_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code NFX,Memo code Blank and Amount Type YA
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 163_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code PA,Memo code R5 and Amount Type A1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 164_Validate for Product Category TP ,Business Event SFU,WMA TRansaction code TA,Memo code Blank and Amount Type AF
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 165_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code NE,Memo code R5 and Amount Type A1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 166_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code TA,Memo code R5 and Amount Type A1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 167_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code KF,Memo code Blank and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 168_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code FNM,Memo code UC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 169_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NFX,Memo code AL and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 170_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code PJ,Memo code G1 and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 171_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code KI,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 172_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code PHF,Memo code UC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 173_Validate for Product Category TP ,Business Event PLN,WMA TRansaction code TC,Memo code IC and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 174_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code KE,Memo code Blank and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 175_Validate for Product Category TP ,Business Event DLD,WMA TRansaction code KI,Memo code Blank and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 176_Validate for Product Category TN ,Business Event PRP,WMA TRansaction code PHP,Memo code R5 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 177_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code PA,Memo code UC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 178_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NA,Memo code G1 and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 179_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NA,Memo code IC and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 180_Validate for Product Category TP ,Business Event PCC,WMA TRansaction code TC,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 181_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code KF,Memo code G2 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 182_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NE,Memo code R5 and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 183_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code PHF,Memo code AL and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 184_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code KE,Memo code G2 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 185_Validate for Product Category TP ,Business Event PLN,WMA TRansaction code TCI,Memo code D1 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 186_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code NE,Memo code Blank and Amount Type J
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 187_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code SR,Memo code Blank and Amount Type GC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 188_Validate for Product Category TP ,Business Event RPP,WMA TRansaction code RA1,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 189_Validate for Product Category TP ,Business Event DBP,WMA TRansaction code NG,Memo code D1 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 190_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code KP,Memo code AL and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 191_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code PA,Memo code IC and Amount Type J
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 192_Validate for Product Category TN ,Business Event CLS,WMA TRansaction code OMB,Memo code D1 and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 193_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SA,Memo code G1 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 194_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code KP,Memo code G1 and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 195_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code TA,Memo code G1 and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 196_Validate for Product Category TN ,Business Event PRP,WMA TRansaction code NF,Memo code IC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 197_Validate for Product Category TN ,Business Event CLS,WMA TRansaction code TW,Memo code G2 and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 198_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code KP,Memo code IC and Amount Type I
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 199_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code NE,Memo code IC and Amount Type I
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 200_Validate for Product Category TP ,Business Event CLS,WMA TRansaction code OMB,Memo code G2 and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 201_Validate for Product Category TP ,Business Event SPA,WMA TRansaction code TWI,Memo code D1 and Amount Type AF
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 202_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code SR,Memo code Blank and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 203_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code KF,Memo code G1 and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 204_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SP,Memo code IC and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 205_Validate for Product Category TP ,Business Event CLS,WMA TRansaction code PJ,Memo code D1 and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 206_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code GH,Memo code IC and Amount Type J
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 207_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NE,Memo code AL and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 208_Validate for Product Category TN ,Business Event CLS,WMA TRansaction code OMB,Memo code G1 and Amount Type E
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 209_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code RA1,Memo code D1 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 210_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code PA,Memo code Blank and Amount Type GC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 211_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code NE,Memo code IC and Amount Type FC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 212_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SA,Memo code R5 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 213_Validate for Product Category TP ,Business Event PLR,WMA TRansaction code PHP,Memo code Blank and Amount Type YA
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 214_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code SR,Memo code Blank and Amount Type J
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 215_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code SR,Memo code Blank and Amount Type FC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 216_Validate for Product Category TP ,Business Event PLN,WMA TRansaction code TCI,Memo code G1 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 217_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code TL,Memo code AL and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 218_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code PA,Memo code IC and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 219_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code GH,Memo code IC and Amount Type FC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 220_Validate for Product Category TN ,Business Event PRP,WMA TRansaction code NF,Memo code G1 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 221_Validate for Product Category TP ,Business Event SPA,WMA TRansaction code GLD,Memo code AL and Amount Type AF
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 222_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code NFX,Memo code Blank and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 223_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code PA,Memo code Blank and Amount Type I
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 224_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SN,Memo code G1 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 225_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code TL,Memo code G2 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 226_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NE,Memo code AL and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 227_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code KF,Memo code Blank and Amount Type FC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 228_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code SC,Memo code IC and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 229_Validate for Product Category TP ,Business Event PLN,WMA TRansaction code TC,Memo code G1 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 230_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code KF,Memo code IC and Amount Type I
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 231_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code PA,Memo code R5 and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 232_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code NA,Memo code D1 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 233_Validate for Product Category TN ,Business Event SUS,WMA TRansaction code RA,Memo code R5 and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 234_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code FNM,Memo code AL and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 235_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code KF,Memo code Blank and Amount Type GC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 236_Validate for Product Category TP ,Business Event SPA,WMA TRansaction code GLD,Memo code IC and Amount Type AF
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 237_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code RA1,Memo code AL and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 238_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code KP,Memo code IC and Amount Type J
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 239_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code TA,Memo code R5 and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 240_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code TW,Memo code UC and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 241_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code TL,Memo code UC and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 242_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code FNM,Memo code AL and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 243_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code GH,Memo code IC and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 244_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code SA,Memo code AL and Amount Type PI
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 245_Validate for Product Category TN ,Business Event PCC,WMA TRansaction code Blank,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 246_Validate for Product Category TN ,Business Event PRW,WMA TRansaction code GH,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 247_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code TC,Memo code G2 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 248_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code GH,Memo code IC and Amount Type I
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 249_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NFX,Memo code AL and Amount Type V
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 250_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code GH,Memo code D1 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 251_Validate for Product Category TP ,Business Event PCC,WMA TRansaction code NE,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 252_Validate for Product Category TN ,Business Event DCF,WMA TRansaction code PA,Memo code G2 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 253_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code SR,Memo code UC and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 254_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code NFX,Memo code G1 and Amount Type O
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 255_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code TWI,Memo code R5 and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 256_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code KP,Memo code G2 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 257_Validate for Product Category TP ,Business Event PRW,WMA TRansaction code KF,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 258_Validate for Product Category TP ,Business Event MAT,WMA TRansaction code FNM,Memo code UC and Amount Type H
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 259_Validate for Product Category TP ,Business Event MAT,WMA TRansaction code FNM,Memo code IC and Amount Type H
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 260_Validate for Product Category TP ,Business Event DWD,WMA TRansaction code FNM,Memo code AL and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 261_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code SR,Memo code G2 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 262_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code KP,Memo code Blank and Amount Type GC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 263_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code NFX,Memo code IC and Amount Type GC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 264_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code GH,Memo code UC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 265_Validate for Product Category TP ,Business Event PRW,WMA TRansaction code NE,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 266_Validate for Product Category TN ,Business Event RPP,WMA TRansaction code RA1,Memo code G1 and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 267_Validate for Product Category TP ,Business Event RPP,WMA TRansaction code RA1,Memo code IC and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 268_Validate for Product Category TP ,Business Event DCF,WMA TRansaction code PJ,Memo code G2 and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 269_Validate for Product Category TP ,Business Event DPC,WMA TRansaction code TAI,Memo code UC and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 270_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code NFX,Memo code IC and Amount Type FC
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 271_Validate for Product Category TP ,Business Event SFU,WMA TRansaction code TAI,Memo code Blank and Amount Type AH
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 272_Validate for Product Category TP ,Business Event DPP,WMA TRansaction code PA,Memo code Blank and Amount Type B
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 273_Validate for Product Category TP ,Business Event InvalidBE,WMA TRansaction code OMB,Memo code AL and Amount Type C
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 274_Validate for Product Category TP ,Business Event PRP,WMA TRansaction code Invalid_Trans_Code,Memo code IC and Amount Type P
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 275_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code Invalid_Trans_Code,Memo code IC and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 276_Validate for Product Category TN ,Business Event InvalidBE,WMA TRansaction code TW,Memo code IC and Amount Type A
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 277_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code PA,Memo code Invalid_Memo_Code and Amount Type B1
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

Scenario: 278_Validate for Product Category TP ,Business Event SUS,WMA TRansaction code NE,Memo code AL and Amount Type Invalid_Amt_Type
Given I validate content from "$DataFile$" against the content from "FSCD"
And I navigate to "(URL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" link
And I capture value "PolicyNumber" as variable "[PolicyNumber]"

