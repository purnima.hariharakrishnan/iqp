﻿@SanityTesting @1 @SanityTesting_SampleApp @test
Feature: Test case for :List extensions
#This is an automated test case generated from IGNITE 
@TestScenario1
Scenario: TestScenario 1
Given I declare rest service with end point "/v2.0/extensions" using method "GET"
When I invoke rest service
Then I validate response present
And I validate response status code with value "500"

@TestScenario2
Scenario: TestScenario 2
Given I declare rest service with end point "/v2.0/extensions" using method "GET"
When I invoke rest service
Then I validate response present
And I validate response status code with value "203"

@TestScenario3
Scenario: TestScenario 3
Given I declare rest service with end point "/v2.0/extensions" using method "GET"
When I invoke rest service
Then I validate response present
And I validate response status code with value "200"

