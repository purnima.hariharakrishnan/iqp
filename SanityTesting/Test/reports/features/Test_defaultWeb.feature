﻿@SanityTesting @Version1 @SanityTesting_SampleApp @Login
Feature: Login
Scenario: 1
And I enter into input field "[Username]" the value "STUDENT"
And I enter into input field "[Password]" the value "Password123"
And I click on "[Submit_button]" button
Then "[Invalid_username]" should have partial text as "Your username is invalid"
And I take screenshot

Scenario: 2
And I enter into input field "[Username]" the value "student"
And I enter into input field "[Password]" the value "Password"
And I click on "[Submit_button]" button
Then "[Invalid_Password]" should have partial text as "Your password is invalid"
And I take screenshot

Scenario: 3
Given I navigate to "https://practicetestautomation.com/practice-test-login/"
And I enter into input field "[Username]" the value "student"
And I enter into input field "[Password]" the value "Password123"
And I click on "[Submit_button]" button
Then "[Validate_Successful Text]" should have text as "Logged In Successfully"
And I take screenshot
And I add wait seconds of "8"
And I click on "[Logout_button]" button
And I add wait seconds of "8"

Scenario: 4
And I enter into input field "[Username]" the value "STUDENT"
And I enter into input field "[Password]" the value "Password123"
And I click on "[Submit_button]" button
Then "[Invalid_username]" should have partial text as "Your username is invalid"
And I take screenshot
And I enter into input field "[Username]" the value "student"
And I enter into input field "[Password]" the value "Password"
And I click on "[Submit_button]" button
Then "[Invalid_Password]" should have partial text as "Your password is invalid"
And I take screenshot

