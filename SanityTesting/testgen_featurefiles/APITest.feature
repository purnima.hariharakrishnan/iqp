@CreateUsers @ForPerformanceTesting
Feature: CreateIQPUsers
@performance @API_Perform11 @APITest1
Scenario: Create users on IQP for PerformanceTesting application 1
Given I set "(BaseURL)" as base URL
And I declare rest service with end point "(EndPoint)" using method "(Method)"
When I append end point with key as "(Append_1)" and value "(Append_1_Value)"
When I append end point with key as "(Append_2)" and value "(Append_2_Value)"
When I append end point with key as "[Append_3]" and value "(User)"
And I set request header property "(Header_1)" with value "(Header_1_Value)"
When I invoke rest service
Then I download the request
And I download the response
Then I download the request
