Feature: Validate search
Scenario: Amazon search 1
Given I navigate to "https://www.amazon.com/"
And I enter into input field "[AmznSearch]" the value "Search"
And I click on "[click]" button
Then I validate that url "{Email Match Pattern}" gives response code "{Email Match Pattern}"
Given I enter into otp field "{Numeric Value Match Pattern}" the otp variable "[sample]"
