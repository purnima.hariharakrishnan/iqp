@OcpTest @testing
Feature: Validate search
@AmazonSearch1 @AmazonSearch1
Scenario: Amazon search 1
Given I navigate to "https://www.amazon.com/"
And I add wait seconds of "2"
And I enter into input field "[AmznSearch]" the value "Iphone"
And I press enter key on "[Searchproduct]"
And Highlevel
And I enter into input field "[AmznSearch]" the value "Laptop"
#-------------------------------------------------------
@AmazonSearch2 @AmazonSearch2 @AmazonSearch2
Scenario: Amazon search 2
Given I navigate to "https://www.amazon.com/"
And I add wait seconds of "2"
And I enter into input field "[Amazon_Search]" the value "laptop"
And I click on "[Searchthe_item]" (button|link|object)
#-------------------------------------------------------
@AmazonSearch3 @AmazonSearch3 @AmazonSearch3
Scenario: Amazon search 3
Given I navigate to "https://www.amazon.com/"
And I add wait seconds of "2"
And I enter into input field "[AmznSearch]" the value "laptop"
