@Test @1 @Test_Test @ByStatus
Feature: Test case for :Finds Pets by status
#This is an automated test case generated from IGNITE 
@TestScenario1
Scenario: TestScenario 1
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "400"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario2
Scenario: TestScenario 2
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "500"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario3
Scenario: TestScenario 3
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "200"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario4
Scenario: TestScenario 4
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "200"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario5
Scenario: TestScenario 5
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "200"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario6
Scenario: TestScenario 6
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "500"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario7
Scenario: TestScenario 7
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "400"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario8
Scenario: TestScenario 8
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "500"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario9
Scenario: TestScenario 9
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "500"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario10
Scenario: TestScenario 10
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "500"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario11
Scenario: TestScenario 11
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "500"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario12
Scenario: TestScenario 12
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "400"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario13
Scenario: TestScenario 13
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "400"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario14
Scenario: TestScenario 14
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "200"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario15
Scenario: TestScenario 15
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "400"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario16
Scenario: TestScenario 16
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "500"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario17
Scenario: TestScenario 17
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "500"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


@TestScenario18
Scenario: TestScenario 18
Given I declare rest service with end point "/pet/findByStatus" using method "GET"

And I append end point with key as "status" and value "SampleQueryParamValue"

When I invoke rest service

Then I validate response present

And I validate response status code with value "400"

And I validate response property "photoUrls" is equal to value "SampleResponsePropertyValue"

And I validate response property "name" is equal to value "SampleResponsePropertyValue"

And I validate response property "status" is equal to value "SampleResponsePropertyValue"


