@OcpTest @testing
Feature: Validate search
Background: background
@AmazonSearch1 @AmazonSearch1 @Amazon1 @Amazon12_Copy1
Scenario: Amazon search 1
Given I check elseif "{adssadsad}" present
When I check if "{Date Pattern : mm-dd-yy}" match the regular expression "#s#"
When I check if "{asASAsaS}" match the regular expression "{AsASasA}"
When I check if "{sfsfdsfdsf}" match the regular expression "$dws$"
When I check elseif "{xvcxvc}" present
When I check if "{Date Pattern : dd/mm/yyyy}" match the regular expression "#d#"
When I check if "{sadsadsadasd}" match the regular expression "#sdfdsfds#"
When I check if "{sadsadsads}" match the regular expression "{sdsadsadsd}"
When I check if "{sfdsfdsfdsf}" match the regular expression "dsfdsfdf"
When I check if "{Phone Number Match Pattern}" match the regular expression "(sfdsfdsfd)"
When I check elseif "{sdsadsadsad}" match the regular expression "{Date Pattern : dd/mm/yyyy}"
When I check if "[sadsadsad]" match the regular expression "(sadsadsd)"
When I check if "{/^[a-zA-Z0-9]+(?:\.[a-zA-Z0-9]+)+@[A-Za-z]+$/}" match the regular expression "sdsadsad"
