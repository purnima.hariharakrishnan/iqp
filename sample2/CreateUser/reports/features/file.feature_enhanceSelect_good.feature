@sample2 @5.10 @sample2_sample @task
Feature: Validate Users Menu
Scenario: Scenario to validate users menu from IQP main menu1
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu2
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu3
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu4
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu5
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu6
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu7
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu8
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu9
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu10
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu11
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu12
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu13
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu14
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu15
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu16
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu17
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu18
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu19
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu20
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu21
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu22
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu23
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu24
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu25
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu26
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu27
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu28
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu29
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu30
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu31
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu32
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu33
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu34
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu35
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu36
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu37
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu38
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu39
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu40
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu41
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu42
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu43
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu44
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu45
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu46
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu47
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu48
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu49
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu50
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu51
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu52
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu53
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu54
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu55
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

Scenario: Scenario to validate users menu from IQP main menu56
Given I navigate to "(URL)"
And I enter into input field "[Username]" the value "(Username)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I take screenshot
And I add wait seconds of "20"
And I click on "[Menu Bar]" link
And I click on "[UsersMenu]" button
And I click on "[AddNewUser]" button
And I enter into input field "[UserName]" the value "(UserNameToCreate)"
And I enter into input field "[EmailID]" the value "(EmailIDToCreate)"
And I enter into input field "[UserID]" the value "(UserIDToCreate)"
And I check the checkbox "[Admin]"
And I select from dropdown "[Portfolio]" the item "62"
And I click on "[Next]" button
And I continue execution on failure of following step
And I click on "[CreateUser]" button
And I capture screenshot
And I click on "[sdfs]" button
And I enter into input field "[hfgh]" the value "jfjfjfj"

